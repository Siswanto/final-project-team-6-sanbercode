@extends('layout.master')

@section('judul')
    {{$thread->judul}}
@endsection

@section('content')
{{-- Tampilan Berita --}}
<section class="content">
  
  <!-- Default box -->
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
            <div class="card">
                <img src="{{asset('gambar/'.$thread->thumbnail)}}" width="500px" alt="...">
                <div class="card-body">
                  <h5>{{$thread->judul}}</h5>
                  <p class="card-text">{!! $thread->content !!}</p>
                </div>
              </div>    
        </div>
    </div>
    {{-- Akhir Tampilan Berita --}}
    
    {{-- Tampilan Komentar --}}
    <div class="row my-2">
      <h2>Komentar</h2>
    
      <div class="col-9" id="mm"></div>
    
    
      @auth
      @forelse ($thread->komentar as $item)
      @if ($item->user_id == Auth::id() )
          <div class="col-12">
            <div class="card">
              <h5 class="card-header">{{$item->user->name}} , {{$item->created_at}}</h5>
              @if ($item->gambar == NULL)
                  
              @else
              <img src="{{asset('gambar/'.$item->gambar)}}" alt="gambar_komentar" width="500px">
              @endif
              <div class="card-body">
                <div class="card-text">{!! $item->content !!}</div>
                <form action="/komentar/{{$item->id}}" method="POST">
                  
                <a href="#"  class="klik_btn btn btn-primary mt-2" data-toggle="modal" data-target="#exampleModal" 
                data-komentar="{{$item->content}}"
                data-id_komentar="{{$item->id}}"
                data-threads_id="{{$thread->id}}"
                data-user_id="{{$item->user_id}}"
                data-url = "/komentar/{{$item->id}}">Edit</a>
                  @method('DELETE')
                  @csrf
                  <input type="submit" class="btn btn-danger mt-2" value="Delete">  
                </form>
              </div>
            </div>
          </div>
      @else
      <div class="col-12">
        <div class="card">
          <h5 class="card-header">{{$item->user->name}} , {{$item->created_at}}</h5>
          @if ($item->gambar == NULL)
                  
          @else
              <img src="{{asset('gambar/'.$item->gambar)}}" alt="gambar_komentar" width="500px">
          @endif
          <div class="card-body">
            <p class="card-text">{!! $item->content !!}</p>
          </div>
        </div>
      </div>
      @endif
      
      @empty
      <div class="col-12">
        <h4>Belum ada komentar</h4>
      </div>
          
      @endforelse
      @endauth
      
      @guest
      @forelse ($thread->komentar as $item)
      <div class="col-12">
        <div class="card">
          <h5 class="card-header">{{$item->user->name}} , {{$item->created_at}}</h5>
          @if ($item->gambar == NULL)
                  
          @else
              <img src="{{asset('gambar/'.$item->gambar)}}" alt="gambar_komentar" width="500px">
          @endif
          <div class="card-body">
            <p class="card-text">{!! $item->content !!}</p>
          </div>
        </div>
      </div>
      
      @empty
      <div class="col-12">
        <h4>Belum ada komentar</h4>
      </div>
          
      @endforelse
      @endguest
    
     
    </div>
    {{-- AkhirTampilan Komentar --}}
    
    
    {{-- Tambah Komentar --}}
    @auth
    <div class="row">
      <div class="col-9">
        <div class="card">
          <h5 class="card-header">Tambah Komentar</h5>
          <div class="card-body">
            <form action="/komentar" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <input type="hidden" value="{{$thread->id}}" name="threads_id">
                  <textarea name="content" placeholder="Masukan Komentar" class="my-editor form-control"></textarea>
                  @error('content')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                  @enderror
              </div>
              <div class="form-group">
                <label for="gambar">Masukan gambar jika perlu</label>
                <input id="gambar" type="file" class="form-control" name="gambar">
                @error('gambar')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
              <button type="submit" class="btn btn-primary">Tambah Komentar</button>
          </form>    
    
          </div>
        </div>
        
      </div>
    </div>
    @endauth
    
    <a href="/thread" class="btn btn-primary my-2">Kembali</a>
    {{-- Akhir Tambah Komentar --}}
    
    {{-- Modal --}}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Komentar</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="url" action="" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
          <div class="modal-body">
            <div class="form-group">
              <input type="text" class="form-control" name="threads_id" id="threads_id" value="" hidden>
              <input type="text" name="id_komentar" id="id_komentar" value="" hidden>
              <input type="text" name="user_id" id="user_id" value="" hidden>
            </div>
            <div class="form-group">
              <textarea name="content" id="edit_komentar" class="form-control"></textarea>
                @error('content')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
              <label for="gambar">Masukan gambar jika ingin update gambar</label>
              <input id="gambar" type="file" class="form-control" name="gambar">
              @error('gambar')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
            {{-- <div>
              <input type="text" name="threads_id" id="threads_id" value="" hidden>
              <input type="text" name="id_komentar" id="id_komentar" value="">
              <input type="text" name="user_id" id="user_id" value="" hidden>
              <textarea name="content" id="edit_komentar"></textarea>
            </div> --}}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    {{-- Akhir Modal --}}

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->

</section>

@endsection



{{-- Script untuk modal --}}
@push('scripts')
<script>
  $(document).ready(function () {
    $(document).on('click', '.klik_btn', function () {
       var data_komentar = $(this).data('komentar');
       var data_id_komentar = $(this).data('id_komentar');
       var data_threads_id = $(this).data('threads_id');
       var data_user_id = $(this).data('user_id');
       var data_url = $(this).data('url')
       $("#edit_komentar").text(data_komentar);
       $("#id_komentar").val(data_id_komentar);
       $("#threads_id").val(data_threads_id);
       $("#user_id").val(data_user_id);
       $("#url").attr("action" , data_url);
    });
  });
  
  </script>

<script src="https://cdn.tiny.cloud/1/2mtek2hflallwz9bn54o0m3qdelswj3b1f7wcgfep4upgy9p/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea.my-editor',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      forced_root_block : false,
   });
  </script>
@endpush


