@extends('layout.master')

@section('judul')
    List Thread
@endsection
@section('content')
<section class="content">
  
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      @auth
<a href="/thread/create" class="btn btn-info my-2">Tambah</a>  
@endauth
    </div>
    <div class="card-body">
      <div class="row">
        @forelse ($thread as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('gambar/'.$item->thumbnail)}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5>{{$item->judul}}</h5>
                  <span class="badge badge-info">{{$item->kategori->nama}}</span>
                  <p class="card-text">{!! Str::limit($item->content ,50) !!}</p>
                  @auth
                  <form action="/thread/{{$item->id}}" method="POST">
                    <a href="/thread/{{$item->id}}" class="btn btn-primary">Read More</a>
                    @if ($item->user_id == Auth::id())
                    <a href="/thread/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                    @method('DELETE')
                    @csrf
                    <input type="submit" class="btn btn-danger" value="Delete">    
                    @endif
                  @endauth
    
                  @guest
                  <a href="/thread/{{$item->id}}" class="btn-sm btn-primary">Read More</a>
                  @endguest
                  
                </div>
              </div>    
        </div>
        @empty
            <h5>Belum Ada Thread</h5>
        @endforelse
        
    </div>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
@endsection
