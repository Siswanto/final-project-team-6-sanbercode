@extends('layout.master')

@section('judul')
    Buat Thread
@endsection
@section('content')
<section class="content">
  
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Buat Thread</h3>
      </div>
      <div class="card-body">
        <form action="/thread" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Judul Thread</label>
                <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul Thread">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label >Content</label>
                <textarea name="content" class="my-editor form-control" id="content" cols="30" rows="10"></textarea>
                @error('content')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label >Kategori</label>
                <select type="text" class="form-control" name="kategori_id">
                    <option value="">--Silahkan Pilih Kategori--</option>
                    @foreach ($kategori as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        
                    @endforeach
                </select>
                @error('kategori')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="thumbnail">Thumbnail</label>
                <input id="thumbnail" type="file" class="form-control" name="thumbnail">
                @error('thumbnail')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>  
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  
  </section>  
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/2mtek2hflallwz9bn54o0m3qdelswj3b1f7wcgfep4upgy9p/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea.my-editor',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      forced_root_block : false,
   });
  </script>
@endpush
