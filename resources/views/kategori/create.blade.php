@extends('layout.master')

@section('judul')
    Tambah Kategori
@endsection
@section('content')
<section class="content">
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Tambah Kategori Forum</h3>
      </div>
      <div class="card-body">
        <form action="/kategori" method="POST">
            @csrf
            <div class="form-group">
                <label >Nama Kategori</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label >Deskripsi Kategori</label>
                <textarea name="deskripsi" cols="30" rows="10" class="form-control"></textarea>
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form> 
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  
  </section>   
@endsection