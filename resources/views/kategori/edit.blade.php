@extends('layout.master')

@section('judul')
    Edit Kategori
@endsection
@section('content')
<section class="content">
  
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Tambah Kategori Forum</h3>
      </div>
      <div class="card-body">
        <form action="/kategori/{{$kategori->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label >Nama Kategori</label>
                <input type="text" class="form-control" name="nama" value="{{$kategori->nama}}" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label >Deskripsi Kategori</label>
                <textarea name="deskripsi" cols="30" rows="10" class="form-control">{{$kategori->deskripsi}}</textarea>
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form> 
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  
  </section>   
@endsection