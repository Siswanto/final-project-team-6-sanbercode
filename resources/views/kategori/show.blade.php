@extends('layout.master')

@section('judul')
    Detail Kategori {{$kategori->nama}}
@endsection

@section('content')

  <!-- Main content -->
  <section class="content">
  
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{$kategori->deskripsi}}</h3>
      </div>
      <div class="card-body">
        <div class="row">
            @forelse ($kategori->thread as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset('gambar/'.$item->thumbnail)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5>{{$item->judul}}</h5>
                      <p class="card-text">{!! Str::limit($item->content , 500) !!}</p>
                      <form action="/thread/{{$item->id}}" method="POST">
                        <a href="/thread/{{$item->id}}" class="btn-sm btn-primary">Read More</a>  
                    </form>
                    </div>
                  </div>    
            </div>
            @empty
                <h5>Tidak Ada Berita</h5>
            @endforelse
            
        </div>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  
  </section>

@endsection