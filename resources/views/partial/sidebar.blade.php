<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
        <img src="{{asset('admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Final Project</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                @auth
                <img src="{{asset('gambar/'.Auth::user()->profile->gambar)}}" class="img-circle elevation-2"
                    alt="User Image">
                @endauth
            </div>
            <div class="info">
                @auth
                <a href="#" class="d-block">{{ Auth::user()->name }} ({{Auth::user()->profile->umur}})</a>
                @endauth
                @guest
                <a href="">Belum Login</a>
                @endguest

            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                @auth
                <li class="nav-item">
                    <a href="/kategori" class="nav-link">
                        <i class="nav-icon fas fa-tasks"></i>
                        <p>
                            Kategori
                        </p>
                    </a>
                </li>
                @endauth

                <li class="nav-item">
                    <a href="/thread" class="nav-link">
                        <i class="nav-icon fas fa-comment-alt"></i>
                        <p>
                            Thread
                        </p>
                    </a>
                </li>
                @auth
                <li class="nav-item">
                    <a href="/profile" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Profil
                        </p>
                    </a>
                </li>
                <li class="nav-item bg-danger">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                @endauth

                @guest
                <li class="nav-item bg-primary">
                    <a href="/login" class="nav-link">
                        <p>Login</p>
                    </a>
                </li>
                @endguest
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
