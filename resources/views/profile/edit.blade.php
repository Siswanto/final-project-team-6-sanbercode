@extends('layout.master')

@section('judul')
Edit Profile
@endsection
@section('content')
<section class="content">

    <!-- Default box -->
    <div class="card">
        {{-- <div class="card-header">
            <h3 class="card-title"></h3>
        </div> --}}
        <div class="card-body">
            <div class="avatar text-center">
                <img class="img-circle" width="200px" src="{{asset('gambar/'.Auth::user()->profile->gambar)}}" alt="">
            </div>
            <form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" value="{{$profile->user->email}}" disabled>
                </div>
                <div class="form-group">
                    <label>Umur</label>
                    <input type="number" class="form-control" name="umur" value="{{$profile->umur}}"
                        placeholder="Masukkan Umur Anda">
                    @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Biodata</label>
                    <textarea name="bio" cols="30" rows="10" class="form-control">{{$profile->bio}}</textarea>
                    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" cols="30" rows="10" class="form-control">{{$profile->alamat}}</textarea>
                    @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="gambar">Foto Profile</label>
                    <input id="gambar" type="file" class="form-control" name="gambar">
                    @error('gambar')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
@endsection
