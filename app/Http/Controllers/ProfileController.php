<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use File;

class ProfileController extends Controller
{
    public function index (){
        $profile = Profile::where('user_id', Auth::id())->first();

        return view('profile.edit' , compact('profile'));
    }

    public function update ($id , Request $request){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);
        

        $profile =  Profile::find($id);

        // $profile->umur = $request->umur;
        // $profile->bio = $request->bio;
        // $profile->alamat = $request->alamat;
        // $profile->user_id = Auth::id();

        // $profile->save();

        // return redirect('/profile');


        if ($request -> has('gambar')) {
            if ($profile->gambar != 'user.png') {
                $path = "gambar/";
                File::delete($path.$profile->gambar);
            }
            // $path = "gambar/";
            // File::delete($path.$profile->gambar);
            $imageName = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('gambar'), $imageName);
            $profile_data = [
                'umur' => $request->umur,
                'bio' => $request->bio,
                'gambar' => $imageName
            ];
        } else {
            $profile_data = [
                'umur' => $request->umur,
                'bio' => $request->bio,
            ];
        }

        $profile->update($profile_data);
        return redirect('/profile');

    }
}
