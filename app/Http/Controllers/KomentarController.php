<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Komentar;
use Illuminate\Support\Facades\Auth;
use File;
class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'content' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $komentar = new Komentar;
        $komentar->threads_id = $request->threads_id;
        $komentar->content = $request->content;
        $komentar->user_id = Auth::id();

        if ($request->has('gambar')) {
            $imageName= time().'.'.$request->gambar->extension();

            $request->gambar->move(public_path('gambar'), $imageName);
            $komentar->gambar = $imageName;
        }
        

        $komentar->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        
        $request->validate([
            'content' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);
        
        $komentar = Komentar::findorfail($id);

        if ($request -> has ('gambar')) {
            // dd($request);
            if ($komentar->gambar != NULL ) {
                // dd($request);
                $path = "gambar/";
                File::delete($path.$komentar->gambar);
            }
            $imageName= time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('gambar'), $imageName);
            $komentar_data = [
                'content' => $request->content,
                'gambar' => $imageName
            ];
            
        } else {
            $komentar_data = [
                'content' => $request->content,
            ];
        }
        
        // dd($komentar_data);
        $komentar->update($komentar_data);
        
        
        
        
        // $komentar = Komentar::findorfail($request->id_komentar);

        // $komentar_data = [
        //     'content' => $request->content,
        //     'threads_id' => $request->threads_id,
        //     'user_id' => $request->user_id
        // ];

        // $komentar->update($komentar_data);
        $url = '/thread/'.$request->threads_id;
        return redirect($url);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $komentar = Komentar::findorfail($id);
        $komentar->delete();

        if ($komentar->gambar != NULL) {
            $path = "gambar/";

            File::delete($path.$komentar->gambar);
        }
        
        $url = '/thread/'.$komentar->threads_id;
        return redirect($url);
    }
}
