<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Thread;
use Illuminate\Support\Facades\Auth;
use File;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $thread = Thread::get();

        return view('thread.index', compact('thread'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('thread.tambah' , compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $imageName= time().'.'.$request->thumbnail->extension();

        $request->thumbnail->move(public_path('gambar'), $imageName);

        $thread = new Thread;
        $thread->judul = $request->judul;
        $thread->content = $request->content;
        $thread->kategori_id = $request->kategori_id;
        $thread->user_id = Auth::id();
        $thread->thumbnail = $imageName;

        $thread->save();

        return redirect ('/thread');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $thread = Thread::find($id);
        // dd($thread->komentar);
        return view('thread.show' , compact('thread'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thread = Thread::find($id);
        $kategori = Kategori::get();
        return view('thread.edit' , compact('thread' , 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        // $imageName= time().'.'.$request->thumbnail->extension();

        // $request->thumbnail->move(public_path('gambar'), $imageName);

        $thread = Thread::findorfail($id);

        if ($request -> has ('thumbnail')) {
            $path = "gambar/";
            File::delete($path.$thread->thumbnail);
            $imageName= time().'.'.$request->thumbnail->extension();
            $request->thumbnail->move(public_path('gambar'), $imageName);
            $thread_data = [
                'judul' => $request->judul,
                'content' => $request->content,
                'kategori_id' => $request->kategori_id,
                'thumbnail' => $imageName
            ];
        } else {
            $thread_data = [
                'judul' => $request->judul,
                'content' => $request->content,
                'kategori_id' => $request->kategori_id
            ];
        }

        $thread->update($thread_data);
        return redirect('/thread');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thread = Thread::findorfail($id);
        $thread->delete();

        $path = "gambar/";

        File::delete($path.$thread->thumbnail);

        return redirect('/thread');
    }
}
