<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $table   = 'threads';
    protected $fillable = ['judul' , 'content' , 'thumbnail' , 'user_id' , 'kategori_id'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar' , 'threads_id');
    }

}

